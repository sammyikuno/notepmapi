import requests
from pprint import pprint
from glob import glob
import json
import os
import re
import time

ACCESS_TOKEN = None
try:
    pass
    ACCESS_TOKEN = os.environ['NOTEPM_API']
    #ACCESS_TOKEN = 'xsXVxn4xq8Svc5J6jnEKlAo61JqPmbxPbYhOIXHqvf64jmRXgVIAlyUS4rdEWGDZ'
except:
    pass

ENDPOINTT_TO_POST = 'https://sammy-dev.notepm.jp/api/v1'
headers = {'Authorization': f'Bearer {ACCESS_TOKEN}'}


class NotePM():
    """ NotePM操作 """

    def __init__(self, note_code: str) -> None:
        self.note_code = note_code
        pass

    def getPagecode(self, title: str = ''):
    #def getPagecode(self, title: str = '') -> None | list[str]:
        """ ページ検索 """
        payload = {
            "q": title,  # 検索する文字列を設定
            "only_title": True,  # タイトルのみで検索する場合に設定
        }
        r_post = requests.get(
            ENDPOINTT_TO_POST+"/pages", headers=headers, json=payload)
        pages = json.loads(r_post._content.decode('utf-8'))["pages"]

        if len(pages):
            for x in pages:
                print(f"get : {x['title']}")
            return pages[0]["page_code"]
        else:
            # サーバにないので新規追加
            p = {
                "note_code": self.note_code,
                "title": title,
                "body": "本文",
                "memo": "memo"
            }
            r_post = requests.post(
                ENDPOINTT_TO_POST+"/pages", headers=headers, json=p)
            print(r_post.status_code)  # 201
            page = json.loads(r_post._content.decode('utf-8'))["page"]
            return page["page_code"]

    def getAttachments(self, _page_code: str):
        """ 添付ファイルを取得 """
        ret = []
        for i in range(1, 5):
            # 検索
            r_post = requests.get(
                ENDPOINTT_TO_POST+"/attachments", headers=headers,
                json={
                    "file_name": "",  # 全て取得
                    "page_code": _page_code,
                    "page": i,
                    "per_page": 100
                })
            # print(r_post.status_code)  # 201
            attachments = json.loads(
                r_post._content.decode('utf-8'))["attachments"]
            ret = ret + attachments
            if not len(attachments) == 100:
                break
        return ret

    def UploadImage(self, _path: str, _page_code: str):
        """ 画像のアップロード """
        print(f"uploading:{_path}")
        files = {
            'page_code': (None, _page_code),
            'file[name]': (None, os.path.basename(_path)),
            'file[contents]': open(_path, 'rb'),
        }
        # time.sleep(0.1)
        r_post = requests.post(
            ENDPOINTT_TO_POST+"/attachments", headers=headers, files=files)
        # print(r_post.status_code)  # 201
        ret = json.loads(
            r_post._content.decode('utf-8'))["attachments"]
        # print(attachments)
        return ret

    def UpdatePage(self, page_code, title:str, body:str, memo:str):
        """ ページの追加or更新 """
        p = {
            "body": body,
        }

        url = f"{ENDPOINTT_TO_POST}/pages/{page_code}"
        r_post = requests.patch(url, headers=headers, data=p)
        if r_post.status_code == 200:
            print(f"ページ更新 : {title}")

    def upMarkDown(self, _file):
        """ マークダウン更新 """
        title = os.path.splitext(os.path.basename(_file))[0]  # + "_fromAPI"
        page_code = self.getPagecode(title)

        if page_code is None:
            print('ページコードが見つからないので終了')
            return False

        text = ""
        with open(_file, "r", encoding='utf-8') as f:
            text = f.read()

        # マークダウン内の画像を取得
        images_in_md = re.findall(
            r"[a-zA-Z0-9_\\/]+\.png|[a-zA-Z0-9_\\/]+\.gif", text)
        images_in_folder = glob(
            fr'{os.path.dirname(_file)}/**/*.*', recursive=True)

        # ローカルファイル探す
        for i, image in enumerate(images_in_md):
            # マークダウン内の画像を走査
            base = os.path.basename(image)
            # print(base)
            find = False
            for p in images_in_folder:
                if os.path.basename(p).upper() == base.upper():
                    # ローカル一致するやつ
                    # print(f"findlocalfile:{p}")
                    images_in_md[i] = {"link_path_str": image, "local_path": p}
                    find = True
                    break
            if not find:
                images_in_md[i] = False

        ####### こっからPNGのURL置換、アップロード #######

        # 添付ファイル取得
        atts = self.getAttachments(page_code)

        max = 360
        count = 0
        # 全てのリンクを置換する
        for x in images_in_md:
            if x and count < max:
                count = count + 1
                print(f"{count} / {len(images_in_md)}")

                # 既存の添付ファイルと比較
                file_id = False
                flag = False
                for a in atts:
                    if a["file_name"] == os.path.basename(x["local_path"]):
                        # 既にアップ済み
                        flag = True
                        break
                if not flag:
                    # サーバに画像がないので新規でアップロード
                    at = self.UploadImage(x["local_path"], page_code)
                    file_id = at["file_id"]
                else:
                    # サーバに画像があるので取得するだけ
                    print(f"アップ無し")
                    file_id = a["file_id"]

                # print(x)
                file_url = f'https://sammy-dev.notepm.jp/private/{file_id}'
                text = text.replace(x["link_path_str"], file_url)
                # print(text)

        # 置換済みマークダウンデータをアップ
        self.UpdatePage(page_code, title, text, "スクリプトからアップされました")


def UpToNotePm(pagebody):
        if ACCESS_TOKEN is None:
            print('環境変数 NOTEPM_API にあなたのトークンを設定してから実行して下さい。')
        else:
            print(ACCESS_TOKEN)

            # ローカルのマークダウンファイルを取得
            # fol = r"u:\sava\document\*.md"
            # md_files = glob(fol)

            

            # アップロード処理を実行
            pagecode = '99ab467b4a' #申請日一覧PC
            note = NotePM(note_code='81d9b52ac2') # ノートのコードを設定すること
            note.UpdatePage(pagecode, 'PC製番申請日情報', pagebody, 'memo')
        pass