import win32com.client
import datetime
import pandas as pd
import NotePmModule
import os
import glob
from contextlib import redirect_stdout

outlook = win32com.client.Dispatch('Outlook.Application').GetNamespace('MAPI')
inbox = outlook.GetDefaultFolder(6) #受信トレイは「6」を指定
 
#受信ボックスの情報を取得
# print('Name: ' + inbox.name)
# print('Count: ' + str(len(inbox.Items)))
accounts = outlook.Folders

############# グローバル変数定義
start_date = datetime.datetime.now()
savepathfol = 'D:\System\python\ShinseibiAutoMail' #絶対パスでフォルダを指定(exe化するとファイル書き込みが厳しくなるので)
#savepathfol = 'D:\work\python\\notepmapi' #絶対パスでフォルダを指定(exe化するとファイル書き込みが厳しくなるので)
openpath = (savepathfol + '\AutoPlayconfig.txt')
delete_str = 'PC画像進捗' #実行前に保存先にあるファイルを消去する場合に指定、この名前が含まれていた場合は削除する
docPath = '' #引数で受け取る文字列

############# 時間指定を行う関数
def GetDateTime():
    # 調査期間を設定するために、外部ファイルから取得
    f = open(openpath, 'r', encoding='UTF-8')
    data = f.read()
    f.close()
    # 予定を抜き出したい期間を指定
    start_date = datetime.datetime.strptime(data, '%Y-%m-%d %H:%M:%S')
    return start_date

############# 上記最新の取得日時より新しい受信で姉崎さんからきているメールかつ件名が「PC画像企画進捗」のものを対象とする
def FolFunc():
    
    folders = inbox.Folders
    for account in accounts:            
        folders = account.Folders
        for folder in folders:
            # print(folder.name)
            if folder.name == '受信トレイ': #受信フォルダだけ
                mails = folder.Items
                for mail in mails:
                    convdatetime = datetime.datetime.strptime(str(mail.ReceivedTime)[:-13], '%Y-%m-%d %H:%M:%S') # datetime型で比較するため
                    # print(start_date)
                    # print(convdatetime)
                    if start_date <= convdatetime: #starttimeから新しいメールのみを調査する
                        if mail.sendername == '姉崎聡': # 差出人を調査
                            if mail.subject == 'PC画像企画進捗': # 件名の調査
                                for attachment in mail.Attachments:
                                    if 'PC画像企画進捗' in attachment.FileName:
                                        # xlsxファイルを削除する
                                        delall = glob.glob(savepathfol + '/' + '*.xlsx')
                                        for p in delall:
                                            os.remove(p)
                                        savepath = savepathfol + '/' + attachment.FileName
                                        attachment.SaveAsFile(savepath) #同階層に保存してみる(要ファイルまでの絶対パス)
                                        return savepath #処理抜け
                    else:
                        return ''

############# 時刻保存テキストを更新
def TextReload():
    f = open(openpath, 'w', encoding='UTF-8')
    f.write(str(datetime.datetime.now())[:-7])
    f.close()

############# エクセルを読み込んで申請日を抜き出す
def ExcelParse():

    input_book = pd.read_excel(docPath, sheet_name=0, index_col=0, usecols = "B, E:F", skiprows=4, dtype=str)
    
    seibanname = ''
    shinseibi = ''
    
    returnstr = ''

    returnlist = []

    returnlist.append('| 製番名         | 申請日          | ')
    returnlist.append('| -------------- | --------------- | ')

    for row in input_book.itertuples():
        
        # 製番名を入れる
        if ('nan' in str(row[0])) != True:
            temp = str(row[0]).split('\n')
            seibanname = temp[0]

        # 申請日を入れる
        if '申請日' in str(row[1]):
            shinseibi = str(row[2])
            #ここで製番名と申請日のセットが確定する
            returnlist.append('| ' + seibanname + ' | ' + shinseibi + ' |')
            seibanname = ''
            shinseibi = ''

    for row in returnlist:
        row = row.replace('\n','→')
        returnstr += row
        returnstr += '\r\n'

    return returnstr

# メイン処理
with redirect_stdout(open(os.devnull, 'w')):
    start_date = GetDateTime()          # 調査開始時刻を調べる(同階層のtextファイルから)
    docPath = FolFunc()                 # 保存したエクセルの絶対パスが入る
    TextReload()                        # テキスト更新
    if docPath != '':                   # エクセルを取得できた場合は、パース処理を行う
        body = ExcelParse()            # エクセルを解析して申請日を抜き出す(文字列で)
        NotePmModule.UpToNotePm(body)  # NotePMへUP
